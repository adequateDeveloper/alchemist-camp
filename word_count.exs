
# Count the words in a file.
#
# Example:
#   $ elixir word_count.exs
#   File to count the words from: word_count.exs
#   68

# Match on everything:
#   except non-word characters   "[^\w]"
#   and multiple blocks of non-word characters  "[^\w]+"
#   and new lines, or single quote characters, or \w characters  "(\\n|'|\\w')""
filter = ~r{(\\n|'|\\w|[^\w'])+}

IO.gets("File to count the words from: ")
  |> String.trim()
  |> File.read!()
  |> String.split(filter)
  |> Enum.filter( &(&1 != "") )
  # |> IO.inspect()
  |> Enum.count()
  |> IO.puts()
