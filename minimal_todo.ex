defmodule MinimalTodo do
  @moduledoc """
  A command line Todo list app that reads CSVs, parses them into an Elixir map,
  and takes commands from user input.

  See also: todo.csv
  .csv file format: Todo,Priority,Urgency,Date Added,Notes

  Notes
  -----
  See: build_string/2 for recursion example
  See: parse_body/1 for Enum.reduce/3 example

  Examples

  """

  @doc """
  Ask for and process the user-supplied Todo csv file.
  """
  def start do
    IO.gets("\nName of .csv file to load: ")
    |> read_file()
    |> parse_body()
    |> show()
    |> command_prompt()
  end

  # Read a .csv file and return the contents.
  # Handle filename errors by prompting again for a filename.
  defp read_file(filename) do
    filename = String.trim filename

    case File.read(filename) do
       {:ok, body} -> body
       {:error, reason} ->
          IO.puts ~s(\nCould not open file "#{filename}",)
          IO.puts ~s("#{:file.format_error reason}"\n)
          start()
    end
  end

  # Parse a file body into header and entry lines, then from that build Todo entry maps.
  # Returns a map of maps with Todo as keys.
  defp parse_body(body) do
    [header_line | entry_lines] = String.split(body, ~r{(\r\n|\r|\n)})

    todo_column_names =
      header_line
      |> parse_header_columns()

    entry_lines
    |> Enum.reduce(%{}, fn next_line, acc ->
        [todo | todo_details] = parse_entry_line(next_line)

        map_of_details =
          Enum.zip(todo_column_names, todo_details)
          |> Enum.into(%{})

        Map.merge(acc, %{todo => map_of_details})
      end)
  end

  # Parse the header string into a column list, skipping the Todo column.
  # Todo column name only required in Todo .csv file. Assumes column name is in first position.
  # Ex: "Todo,Priority,Urgency,Date Added,Notes" => ["Priority", "Urgency", "Date Added", "Notes"]
  defp parse_header_columns(header_line) do
    header_line
    |> String.split(",")
    |> tl
  end

  # return a string of header column names
  defp header_column_names do
    "Todo,Priority,Urgency,Date Added,Notes"
  end

  # Parse the todo entry string into a entry list.
  # Ex: "See doctor,1,3,Dec 05,It might be nothing" => ["See doctor", "1", "3", "Dec 05", "It might be nothing"]
  defp parse_entry_line(entry) do
    entry
    |> String.split(",")
  end

  # Show the Todos
  defp show(data) do
    IO.puts("\nTodos: (Todo, Date Added, Notes, Priority, Urgency)\n")

    # feed the keys of the embedded maps
    Map.keys(data)
    |> Enum.each(fn next_key ->
        # get the map of details associated with the todo key
        todo_details = Map.fetch!(data, next_key)

        # get the list of detail values
        values = Map.values(todo_details)

        # place the todo key at the front of the list then build & print the output string
        [next_key] ++ values
        |> build_string("")
        |> print_todo()
      end)

    data
  end

  defp print_todo(todo) do
    indent = "  "
    IO.puts indent <> todo
  end

  # build a Todo ouput string from a list of strings
  defp build_string([], acc), do: acc

  defp build_string([head | tail], acc) do
    if String.length(acc) > 0 do
      build_string(tail, acc <> ", #{head}")
    else
      build_string(tail, acc <> "- #{head}")
    end
  end

  # Get the next instruction from the user
  defp command_prompt(data) do
    command = prompt_for_command()

    case command do
      "r" -> data
        |> show()
        |> command_prompt()
      "a" -> data
        |> add()
        |> show()
        |> command_prompt()
      "d" -> data
        |> delete()
        |> show()
        |> command_prompt()
      "l" -> start()
      # "s" ->
      "q" -> IO.puts "Goodbye!"
      _other -> data
        |> command_prompt()
    end
  end

  # display a user prompt for the next command
  defp prompt_for_command() do
    prompt = """
    Type the first letter of the desired command:
    R)ead Todos    A)dd a Todo    D)elete a Todo    L)oad a .csv file    S)ave a .csv file    Q)uit
    """
    IO.gets("\n" <> prompt)
    |> String.trim()
    |> String.downcase()
  end

  # delete a Todo entry from the data
  defp delete(data) do
    entry =
      IO.gets("Enter the Todo to delete:\n")
      |> String.trim()

    IO.puts ~s(Deleting Todo "#{entry}"...)
    new_data = Map.drop(data, [entry])

    new_data
  end

  # add a Todo entry to the data
  defp add(data) do
    entry_line = prompt_for_new_entry()

    [todo | todo_details] = parse_entry_line(entry_line)

    todo_column_names =
      header_column_names()
      |> parse_header_columns()

    map_of_details =
      Enum.zip(todo_column_names, todo_details)
      |> Enum.into(%{})

    new_data = Map.merge(data, %{todo => map_of_details})

    new_data
  end

  # display a user prompt to add an entry
  defp prompt_for_new_entry() do
    prompt = """
    Enter a new Todo in the form of:
    Todo (String),Priority (Integer),Urgency (Integer),Date Added (String),Notes (String)
    \nExample:
    A todo,3,1,April 01,Notes on this todo\n
    """

    IO.gets("\n" <> prompt)
    |> String.trim()
  end

end
