defmodule GuessingGame do
  @moduledoc """
  Lesson 1: The Guessing Game. A simple guessing game.

  The player will think of a number between a range of numbers,
  and the program will repeatedly guess what number the player is thinking of.

  After each guess by the program the player will respond with either:
    'bigger':  if the guess was too small,
    'smaller': if the guess was too large,
    'yes':     if the guess was correct.
    Any other user response emits a help message

  The program insures the two values supplied by the user to start the game are integers.
  If the two values are a high - low instead of low - high, the program switches the two values
  and continues playing.

  Examples
    $ iex
    > c "guessing_game.ex"
    > GuessingGame.guess 1, "50"
    Error: integers required for input values
    > GuessingGame.guess 1, 50
    Hmmm... are you thinking of 25?
    > GuessingGame.guess 50, 1
    Hmmm... are you thinking of 25?
  """

  def guess(first_value, second_value) when not is_integer(first_value) or not is_integer(second_value) do
    IO.puts "Error: integers required for input values"
  end

  def guess(first_value, second_value) when second_value < first_value, do: guess(second_value, first_value)

  def guess(low, high) do
    response = IO.gets("Hmmm... are you thinking of #{mid(low, high)}?\n")

    case String.trim(response) do
       "bigger" ->
          new_low =
            mid(low, high)
            |> bigger(high)
          guess(new_low, high)

       "smaller" ->
          new_high =
            mid(low, high)
            |> smaller(low)
          guess(low, new_high)

       "yes" -> "I knew I could guess your number!"

       _other ->
        IO.puts(~s{\nResponse options: "bigger" | "smaller" | "yes"\n})
        guess(low, high)
    end
  end

  defp mid(low, high), do: div(low + high, 2)

  defp bigger(mid, high), do: min(high, mid + 1)

  defp smaller(mid, low), do: max(low, mid - 1)
end
